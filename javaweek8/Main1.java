package javaweek8;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class Main1 {
	public static <E> void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		
		int m;
		int n;
  while(sc.hasNext()){
	  String l1 = sc.next();
	  if(l1.equals("quit"))
		  break;
	
   switch (l1) {
  
case "Integer":
	 int sum=0;
	  m =sc.nextInt();
	  n = sc.nextInt();
	System.out.println("Integer Test");
	GeneralStack<Integer> stack = new ArrayListGeneralStack<> ();
	for (int i1 = 0; i1 < m; i1++) {
		int k =  sc.nextInt();
		System.out.println("push:"+stack.push(k));
	}
	for (int i = 0; i < n; i++) {
		
		
			System.out.println("pop:"+stack.pop());
		
	}
	System.out.println(stack.toString());
	while(!stack.empty())
	{
		sum=sum+stack.pop();
		
	}
	System.out.println("sum="+sum);

	System.out.println(stack.getClass().getInterfaces()[0]);
	break;
case "Double":
	  m =sc.nextInt();
	  n = sc.nextInt();
	 double sum1=0;
	
	System.out.println("Double Test");
	GeneralStack<Double> stack1 = new ArrayListGeneralStack<> ();
	for (int i1 = 0; i1 < m; i1++) {
		double k =  sc.nextDouble();
		System.out.println("push:"+stack1.push(k));
	}
	for (int i = 0; i < n; i++) {
		   
		System.out.println("pop:"+stack1.pop());
		
	}
   
	System.out.println(stack1.toString());
	while(!stack1.empty())
	{
		sum1=sum1+stack1.pop();
		
	}
	
	
	System.out.println("sum="+sum1);
	System.out.println(stack1.getClass().getInterfaces()[0]);
	break;
case "Car":
	  m =sc.nextInt();
	  n = sc.nextInt();
	
	System.out.println("Car Test");
	GeneralStack<Car>  stack11 = new ArrayListGeneralStack<> ();
	for (int i1 = 0; i1 < m; i1++) {
		int k =  sc.nextInt();
		String l =sc.next();
		Car a = new Car(k,l);
		System.out.println("push:"+stack11.push(a));
	}
	for (int i = 0; i < n; i++) {
		Car a =stack11.pop();
       
		System.out.println("push:"+a);
	}
	System.out.println(stack11.toString());
	while(!stack11.empty())
	{
		System.out.println(stack11.pop().getName());
	}
	System.out.println(stack11.getClass().getInterfaces()[0]);
	break;
	default:
		break;
	
   }
}
}
}
interface GeneralStack<E>{
	E push(E item);           
	E pop();                 
	E peek();               
	public boolean empty();
	public int size();   
}
class ArrayListGeneralStack<E> implements GeneralStack<E>{

	private  List<E> list = new ArrayList<E>();
	@Override
	public E push(E item) {
		// TODO Auto-generated method stub
		if (item == null)
			return null;
		list.add(item);
		return item;
		
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		if (list.size() == 0)
			return null;

		return list.remove(list.size() - 1);
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		if (list.size() == 0)
			return null;
		return list.get(list.size() - 1);
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		if (list.size() == 0)
			return true;
		return false;
		
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public String toString() {
		return list.toString();
	}
	
}
class Car{
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	public Car(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
}