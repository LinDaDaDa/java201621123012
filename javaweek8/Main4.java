package javaweek8;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javaweek6.gouwuche;

enum Gender {
	Man, Woman
}

public class Main4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new Student(10L, "A", 10, Gender.Man, false);
		new Student(20L, "B", 20, Gender.Man, true);
		Student.stu.add(null);
//		// List<Student> Student1 = Student.stu.stream().filter
//		(e -> e != null && e.getId() > 15L && e.getName().equals("B") && e.getAge() > 10 && e.getGender() == Gender.Man
//				&& e.isJoinsACM() == true).collect(Collectors.toList());
		List<Student> Student2 = Student.search(15L, "B", 15, Gender.Man, true);
		System.out.println(Student2);
		// System.out.println(Student1);
	}
}

class Student {
	private Long id;
	private String name;
	private int age;
	private Gender gender;// 枚举类型
	private boolean joinsACM; // 是否参加过ACM比赛
	static List<Student> stu = new ArrayList<Student>();

	public Student(Long id, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
		stu.add(this);

	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM="
				+ joinsACM;
	}

	public static List<Student> search(Long id, String name, int age, Gender gender, boolean joinsACM) {
		List<Student> stu1 = new ArrayList<Student>();
		for (Student student : stu) {
			if (student == null)
				continue;
			else if (student.id > id && student.name.equals(name) && student.age > age && student.gender.equals(gender)
					&& student.joinsACM == joinsACM)
				stu1.add(student);

		}

		return stu1;

	}
}

//
// public Long getId() {
// return id;
// }
//
//
// public void setId(Long id) {
// this.id = id;
// }
//
//
// public String getName() {
// return name;
// }
//
//
// public void setName(String name) {
// this.name = name;
// }
//
//
// public int getAge() {
// return age;
// }
//
//
// public void setAge(int age) {
// this.age = age;
// }
//
//
// public Gender getGender() {
// return gender;
// }
//
//
// public void setGender(Gender gender) {
// this.gender = gender;
// }
//
//
// public boolean isJoinsACM() {
// return joinsACM;
// }
//
//
// public void setJoinsACM(boolean joinsACM) {
// this.joinsACM = joinsACM;
// }
//
// }
