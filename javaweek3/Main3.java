
import java.util.Arrays;
import java.util.Scanner;


public class Main3 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.nextLine());
		Shape[] list = new Shape[n];

		for (int i = 0; i < n; i++) {
			String str = sc.nextLine();
			if (str.equals("rect")) {
				String[] strlist = sc.nextLine().split(" ");
				list[i] = new Rectangle(Integer.parseInt(strlist[0]), Integer.parseInt(strlist[1]));
			} else {
				int r = Integer.parseInt(sc.nextLine());
				list[i] = new Circle(r);
			}
		}
		System.out.println(Shape.sumAllPerimeter(list));
		System.out.println(Shape.sumAllArea(list));
		System.out.println(Arrays.deepToString(list));

		for (Shape temp : list) {
			System.out.println(temp.getClass() + "," + temp.getClass().getSuperclass());
		}

	}

}





abstract class Shape {

	abstract double getArea();

	abstract double getPerimeter();

	public static double sumAllArea(Shape[] list) {
		double sumAllArea = 0;
		for (Shape temp : list)
			sumAllArea += temp.getArea();
		return sumAllArea;

	}

	public static double sumAllPerimeter(Shape[] list) {
		double sumAllPerimeter = 0;
		for (Shape temp : list)
			sumAllPerimeter += temp.getPerimeter();
		return sumAllPerimeter;

	}

}

class Circle extends Shape{
	
	private static final double  PI=3.14;
	
	private int radius;

	public Circle(int radius) {
		super();
		this.radius = radius;
	}

	public double getArea() {
		return (PI*this.radius*this.radius);
	}

	public  double getPerimeter() {
		return (PI*this.radius*2);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
	
}
 class Rectangle extends Shape {

	private int width;
	private int length;

	public Rectangle(int width, int length) {
		super();
		this.width = width;
		this.length = length;
	}

	public double getArea() {
		return this.length * this.width;
	}

	public double getPerimeter() {
		return 2 * (width + length);
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + "]";
	}

}

