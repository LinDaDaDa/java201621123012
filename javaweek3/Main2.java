package javaweek3;

import java.util.Arrays;
import java.util.Scanner;



public class Main2 {

	public static void main(String[] args) {
		Rectangle []R=new Rectangle[2];
		Scanner sc = new Scanner(System.in);
		
		R[0]= new Rectangle(sc.nextInt(),sc.nextInt());
        R[1]= new Rectangle(sc.nextInt(),sc.nextInt());
        Circle []C=new Circle[2];
        C[0] = new Circle(sc.nextInt());
        C[1] = new Circle(sc.nextInt());
        System.out.println(R[0].getPerimeter()+R[1].getPerimeter()+
                C[0].getPerimeter()+C[1].getPerimeter());
        System.out.println(R[0].getArea()+R[1].getArea()+
                C[0].getArea()+C[1].getArea());
        System.out.println(Arrays.deepToString(R));
        System.out.println(Arrays.deepToString(C));
	}
}
	class Rectangle {
		private int width;
	    private int length;
	    public Rectangle(int width,int length)
	    {
	        this.width=width;
	        this.length=length;
	    }

	    public int getPerimeter()
	    {
	        return (width+length)*2;
	    }

	    public int getArea(){
	        return width*length;
	    }
	    public String toString() {
	        return "Rectangle [" +"width=" + width +", length=" + length +']';
	                
	                
	                
	    }
	
	    
	    
	}

class Circle{
    private int radius;
    private double i;

    public Circle(int radius)
    {
        this.radius=radius;
    }

    public int getPerimeter()
    {
     
        return   (int) (Math.PI*2*radius);
    }

    public int getArea(){
    	 return   (int) (Math.PI*radius*radius);
    }

   
    public String toString() {
        return "Circle [" +"radius=" + radius + ']';
                
               
    }
}

