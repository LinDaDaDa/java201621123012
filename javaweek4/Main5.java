package javaweek4;

public class Main5 {

	public static void main(String[] args) {
		Fruit fruit1 = new Fruit ("c");
		Fruit fruit2 = new Fruit ("C");
		
		System.out.println(fruit1.equals(fruit2));
		System.out.println(fruit1.toString());
	}

}
class Fruit{
	private String name;

	public Fruit( String name) {
		super();
        this.name = name;
	}


	@Override
	public String toString() {
		return  super.toString()+" [name=" + name + "]";
	}

	
	public boolean equals(Fruit obj) { 
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;   
        if (name.equalsIgnoreCase(obj.name))
            return true;
        return false;
    } 
	}