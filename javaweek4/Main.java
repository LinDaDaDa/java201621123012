package javaweek4;

import java.util.Arrays;
import java.util.Scanner;

class PersonOverride{
    private String name;
    private boolean gender;
    private int age;
   
  
    public PersonOverride(){
        this("default",1,true);
    }
    public PersonOverride(String name,int age , boolean gender) {
        super();
        this.name = name;
        this.gender = gender;
        this.age = age;
    }
    @Override
    public String toString() {
        return  name + "-" + age + "-" + gender;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PersonOverride other = (PersonOverride) obj;
        if (age != other.age)
            return false;
        if (gender != other.gender)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
    
    
}
public class Main {
    public static void main(String[] args) {
    	while(true){
    	 int q=0;
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        PersonOverride[] person1 = new PersonOverride[n];
        for (int i = 0; i < n; i++) {
            person1[i]=new PersonOverride();
            
        }
        int n1 = Integer.parseInt(sc.nextLine());
        PersonOverride[] person3 = new PersonOverride[n1];
        PersonOverride[] person2 = new PersonOverride[n1];
        
        for (int i = 0; i < n1; i++) {
        	
            person3[i]=new PersonOverride(sc.next(),sc.nextInt(),sc.nextBoolean());
            
 
        }
        for (int i = 0; i < n1; i++){
        	if(q==0){
        		
        		person2[q]=person3[i];
        		q++;
        	}
        	else{
        		  for (int j = 0; j < q; j++) {
					
				
        		
					if(person3[i].equals(person2[j]))
						break;
					else{
						person2[q]=person3[i];
						q++;
					}
				
        	}
        }}
        for (int i1 = 0; i1 < n; i1++) {
            System.out.println(person1[i1].toString());
        }
        for (int i1 = 0; i1 < q; i1++) {
            System.out.println(person2[i1].toString());
        }
        System.out.println(q);
        System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
    
    	}
    
    }
}

