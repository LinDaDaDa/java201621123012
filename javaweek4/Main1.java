 public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Employee)) return false;
        if (!super.equals(obj)) return false;

        Employee employee = (Employee) obj;
        DecimalFormat df1 = new DecimalFormat("#.#");
        DecimalFormat df2 = new DecimalFormat("#.#");

        if (!df1.format(employee.salary).equals(df2.format(salary))) return false;
        return company != null ? company.equals(employee.company) : employee.company == null;
    }