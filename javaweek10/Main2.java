package javaweek10;

public class Main2 {

}
class Account {
	private int balance;

	public int getBalance() {
		return balance;
	}
	
	public Account(int balance) {
		super();
		this.balance = balance;
	}

	public  void deposit(int money){
		synchronized(this){
		this.balance = balance+money;
		
		}
	}

	public  void withdraw(int money){
		synchronized(this){
			this.balance = balance-money;
		}
			
				Thread.yield();		
				
			
	}

	
}