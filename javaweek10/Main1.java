package javaweek10;

public class Main1 {

}
class Account {
	private int balance;

	public int getBalance() {
		return balance;
	}
	
	public Account(int balance) {
		super();
		this.balance = balance;
	}

	public synchronized void deposit(int money){
		this.balance = balance+money;
	}

	public synchronized void withdraw(int money){
		this.balance = balance-money;
	}

	
}