package javaweek10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PrintTask task = new PrintTask(Integer.parseInt(sc.next()));
        Thread t1 = new Thread(task);
        t1.start();
        sc.close();
    }
}
class PrintTask implements Runnable{
  int n;
	public PrintTask(int parseInt) {
		// TODO Auto-generated constructor stub
		this.n=parseInt;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < n; i++) {
			System.out.println(i);
		}
		System.out.println(Thread.currentThread().getName());
	}
	
}