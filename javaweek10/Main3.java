package javaweek10;

import java.util.Scanner;

public class Main3 {
	 public static void main(String[] args) throws InterruptedException {
	        MonitorTask task1 = new MonitorTask();
	        Thread t1 = new Thread(task1);
	        t1.start();
	        Scanner sc = new Scanner(System.in);
	        while (sc.hasNext()) {
	            String word = sc.next();
	            if (word.equals("stop")) {
	                task1.stopMe();
	            } else if (word.equals("quit")) {
	                break;
	            } else {
	                if (t1.isAlive())
	                   task1.sendWord(word);
	            }
	            Thread.sleep(10);
	        }
	        System.out.println(t1.isAlive());
	        task1.stopMe();
	        sc.close();
	    }
}
class  MonitorTask implements Runnable{
	private volatile boolean  flag = false;
	private String word;
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!flag){
			
			if(word!=null){
//				System.out.println(word);
			if(word.contains("alien"))
			System.out.println(Thread.currentThread().getName()+" found alien in "+word);
			
			}
			word=null;
			Thread.yield();
		}
		
		System.out.println(Thread.currentThread().getName() + " stop");
	}

	public void stopMe() {
		// TODO Auto-generated method stub
		
		flag = true;
	}

	public void sendWord(String word) {
		// TODO Auto-generated method stub
		this.word=word;
	}
	
}