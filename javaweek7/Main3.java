package javaweek7;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main3 {
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in

	);
	int n;
	Integer k;
	n=sc.nextInt();
	ArrayIntegerStack arrayIntegerStack = new ArrayIntegerStack();for(
	int i1 = 0;i1<n;i1++)
	{
		k = sc.nextInt();
		System.out.println(arrayIntegerStack.push(k));
	}
	System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
	System.out.println(arrayIntegerStack.toString());
	n=sc.nextInt();
	for(
	int i = 0;i<n;i++)
	{
		System.out.println(arrayIntegerStack.pop());
	}System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
	System.out.println(arrayIntegerStack.toString());
}
}


interface IntegerStack {
	public Integer push(Integer item);

	public Integer pop();

	public Integer peek();

	public boolean empty();

	public int size();
}

class ArrayIntegerStack implements IntegerStack {
	private  List<Integer> b = new ArrayList();

	public Integer push(Integer item) {
		if (item == null)
			return null;
		b.add(item);
		return item;
	}

	@Override
	public Integer pop() {

		if (b.size() == 0)
			return null;

		return b.remove(b.size() - 1);

	}

	@Override
	public Integer peek() {
		if (b.size() == 0)
			return null;
		return b.get(b.size() - 1);
	}

	@Override
	public boolean empty() {
		if (b.size() == 0)
			return true;
		return false;
	}

	@Override
	public int size() {

		return b.size();
	}
	public String toString(){
		return b.toString();
	}

}