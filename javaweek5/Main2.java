package javaweek5;



import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable2{
	String name;
	 int age;
	
	public PersonSortable2(String name, int age) {
		
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return  name + "-" + age ;
	}

	



	
}
class NameComparator implements Comparator<PersonSortable2> {

	@Override
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return  o1.name.compareTo(o2.name);
	}
	
	
}
class AgeComparator implements Comparator<PersonSortable2> {

	@Override
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		return o1.age-o2.age;
	}
	
	
}

public class Main2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n= sc.nextInt();
		PersonSortable2[] stus = new PersonSortable2[n];
	for (int i = 0; i < n; i++) {
		stus[i] = new PersonSortable2(sc.next(),sc.nextInt());
	}
	
		Arrays.sort(stus,new NameComparator());
		System.out.println("NameComparator:sort");
		for (int i = 0; i < n; i++) {
			System.out.println(stus[i]);
		}
	
		Arrays.sort(stus,new AgeComparator());
		System.out.println("AgeComparator:sort");
		for (int i = 0; i < n; i++) {
		System.out.println(stus[i]);
	}
		
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
}
	}
