package javaweek5;



import java.util.Arrays;
import java.util.Scanner;

class PersonSortable implements Comparable<PersonSortable>{
	private String name;
	private int age;
	public int compareTo(PersonSortable o){
		if( name.compareTo(o.name)==0){
			return age-o.age;
		};
		
		return name.compareTo(o.name);
	}
	
	public PersonSortable(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return  name + "-" + age ;
	}


	
}

public class Main1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n= sc.nextInt();
		PersonSortable[] stus = new PersonSortable[n];
	for (int i = 0; i < n; i++) {
		stus[i] = new PersonSortable(sc.next(),sc.nextInt());
	}
		Arrays.sort(stus);
		for (int i = 0; i < n; i++) {
		System.out.println(stus[i]);
	}
		System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));
}
}
