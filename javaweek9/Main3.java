package javaweek9;

import java.util.EmptyStackException;

public class Main3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
class ArrayIntegerStack implements IntegerStack{
    private int capacity;
    private int top=0;
    private Integer[] arrStack;
    /*其他代码*/
    /*你的答案，即3个方法的代码*/
	@Override
	public Integer push(Integer item) {
		// TODO Auto-generated method stub
	
		if(item==null)
			return null;
		
			else if(arrStack[capacity-1] != null)
				throw new FullStackException();
		
		
		
		for(int i=0; i<capacity;i++)
		{
			if(arrStack[i]==null){
				arrStack[i] = item;
				break;
			}
		}
		return item;
	}
	@Override
	public Integer pop() {
		// TODO Auto-generated method stub
		int i= 0;
		for(i=0; i<capacity;i++)
		{
			if(arrStack[i]==null){
				
				break;
				
			}
		}
		
		

	
		if(arrStack[i-top-1]==null)
			throw new EmptyStackException();
		top++;
		return arrStack[i-top];
		
		
		
		
	}
	public Integer peek() {
		// TODO Auto-generated method stub
		
		int i=0;
		for(i=0; i<capacity;i++)
		{
			if(arrStack[i]==null){
				
				break;
				
			}
		}
	
	
		
		
		if(arrStack[i-1-top]==null)
			throw new EmptyStackException();
		return arrStack[i-1-top];
	
	
}
}
interface IntegerStack {
	public Integer push(Integer item); //如果item为null，则不入栈直接返回null。如果栈满，抛出FullStackException（系统已有的异常类）。
	public Integer pop();              //出栈。如果栈空，抛出EmptyStackException，否则返回
	public Integer peek(); 
}
