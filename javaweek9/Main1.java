package javaweek9;

import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int flag = 0;
		int d = 0;
		int e = 0;
		int i = sc.nextInt();
		double[] a = new double[i];
		for (int j = 0; j < a.length; j++) {
			a[j] = sc.nextInt();
		}

		while (flag == 0) {
			try {
				d = sc.nextInt();
				e = sc.nextInt();
			} catch (Exception e1) {
				flag = 1;
			}
			if (flag == 0) {
				try {
					double c = ArrayUtils.findMax(a, d, e);
					Integer.parseInt(s);
					System.out.println(c);
				} catch (Exception e1) {
					System.out.println(e1);
				}
                
			}
		}
		try {
			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class, int.class, int.class));
			
		} catch (Exception e1) {
		}
      
	}
}

class ArrayUtils {
	public static double findMax(double[] arr, int begin, int end) throws IllegalArgumentException {

		if (begin >= end) {
			IllegalArgumentException e = new IllegalArgumentException("begin:" + begin + " >= " + "end:" + end);

			throw e;
		}
		if (begin < 0) {
			IllegalArgumentException e = new IllegalArgumentException("begin:" + begin + " < 0");
			throw e;
		}
		if (end > arr.length) {
			IllegalArgumentException e = new IllegalArgumentException("end:" + end + " > "+"arr.length");
			throw e;
		}
		int max = (int) arr[begin];
		for (int i = begin; i < end; i++) {
			if (max < arr[i])
				max = (int) arr[i];
		}

		return (double) max;

	}
}
