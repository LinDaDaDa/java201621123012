package javaweek9;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    Resource resource = null;
	    try{
	        resource = new Resource();
	        resource.open(sc.nextLine());
	         /*这里放置你的答案*/
          System.out.println("resource open success");
	    }catch(Exception e){
	    	System.out.println(e);
	    }
	    finally {
			try {
				 resource.close();
				 System.out.println("resource release success");
			} catch (Exception e2) {
				// TODO: handle exception
				System.out.println(e2);
			}
		}
	       sc.close();

	}
}
