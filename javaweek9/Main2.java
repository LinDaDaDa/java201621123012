package javaweek9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte[] content = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("testfis.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("找不到此文件，请重新输入");
			e.printStackTrace();
		}
		int bytesAvailabe = 0;
		try {
			bytesAvailabe = fis.available();
			if(bytesAvailabe>0){
			    content = new byte[bytesAvailabe];
			    fis.read(content);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("读取失败");
			e.printStackTrace();
		}//获得该文件可用的字节数
	//创建可容纳文件大小的数组
		  finally{
			  try {
				fis.close();
			} catch (Exception e2) {
				// TODO: handle exception
				System.out.println("关闭失败");
			}
		  }
		
		System.out.println(Arrays.toString(content));//打印数组内容
	

	try (FileInputStream fis =  new FileInputStream("testfis.txt")){  
		fis = new FileInputStream("testfis.txt");
		bytesAvailabe = fis.available();
		if(bytesAvailabe>0){
		    content = new byte[bytesAvailabe];
		    fis.read(content);
	} }catch (Exception e) {  
	    e.printStackTrace();  
	}
	
}
}