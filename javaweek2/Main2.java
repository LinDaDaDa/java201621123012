package javaweek2;

import java.util.*;
public class Main2 {
    private static Scanner in;
    public static void main(String[] args)
    {
        in=new Scanner(System.in);
        int n;
        while(true)
        {
            n=in.nextInt();
            String s[][]=new String[n][];

            for(int i=0;i<n;i++)
            {
                s[i]=new String[i+1];
                for(int j=0;j<=i;j++)
                {
                    s[i][j]=(i+1)+"*"+(j+1)+"="+(i+1)*(j+1);
                }
            }
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<=i;j++)
                {
                    if (j==i)
                        System.out.println(s[i][j]);
                    else
                        System.out.printf("%-7s",s[i][j]);
                }
            }
            System.out.println(Arrays.deepToString(s));

        }
    }
}
