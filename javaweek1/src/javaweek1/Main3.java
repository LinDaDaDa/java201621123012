package javaweek1;

import java.util.Scanner;
public class Main3 {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()){
			double a = sc.nextDouble();
			if(a<0)
			 System.out.printf("%.6f\n",Math.sqrt(a));
			else{
				double b=Math.sqrt(a);
				for (double i = 0; ; i=i+0.0001) {
					double c=Math.pow(i, 2);
					if(c<a&&(a-c>0.0001||a-c<-0.0001))
						continue;
					else
				{
						System.out.printf("%.6f\n",i);
						break;
				}
					
					
				}
				
			}
		}
		
		
	}
}
