package javaweek6;
import java.text.DecimalFormat;
import java.util.Scanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;



public class Main4 {
	
	
	public static void main(String[] args) {
		
		ArrayList<Person> personList = new ArrayList<Person>();
		ArrayList<Student> stuList = new ArrayList<Student>();
		ArrayList<Employee> empList = new ArrayList<Employee>();
		
		Scanner sc=new Scanner(System.in 

 

);
		boolean key=true;
		while(key) {
		switch(sc.next()) {
		
			case "s":Student s=new Student(sc.next(),sc.nextInt(),sc.nextBoolean(),sc.next(),sc.next());
				personList.add(s);
				break;
				
			case "e":Employee e=new Employee(sc.next(),sc.nextInt(),sc.nextBoolean(),sc.nextDouble(),new Company(sc.next()));
				personList.add(e);
				break;
				
			default: key=false;
			}	
		}
		
		Collections.sort(personList,new Comparator<Person>(){

			public int compare(Person p1,Person p2) {
	
				if(p1.name 

 

.compareTo(p2.name 

 

)==0){
					if(p1.age>p2.age)
						return 1;
					else if(p1.age<p2.age)
						return -1;
					else
						return 0;
				}
					
				return p1.name 

 

.compareTo(p2.name 

 

);
			}
		});

			
//		Arrays.sort(personList,new NAComparator());
		for(Person p:personList){
			
			System.out.println(p.toString());
		}
		
		if(!sc.next().equals("exit")){
			for(Person p:personList){
				int flag=0;
				
				if((p instanceof Student)==true){
					for(Student s :stuList){
						
						if((p.equals((s)) ==true)){
							flag=1;
							break;	
						}	
							
					}
					if(flag==0){
						stuList.add((Student)p);
						
					}
					else
						flag=0;
				}
					
				else{
					for(Employee e:empList)
						if(p.equals((e)) ==true){
							flag=1;
							break;
						}
						
					if(flag==0)
						empList.add((Employee)p);
					else
						flag=0;		
				}			
					
			}
			System.out.println("stuList");
			for(int i=0;i<stuList.size();i++)
				System.out.println(stuList.get(i).toString());
			System.out.println("empList");
			for(int i=0;i<empList.size();i++)
				System.out.println(empList.get(i).toString());
			
		}

	}
}
	




class Person{
	
	String name;
	int age;
	boolean gender;
	
	public Person(String name, int age, boolean gender) {
		this.name 

 

=name;
		this.age=age;
		this.gender=gender;
	}
	
	@Override
	public boolean equals(Object o) {
		if(this==o)
			return true;
		
		if(!(o instanceof Person))
			return false;
		Person p=(Person)o;
		if(name!=p.name 

 

||age!=p.age||gender!=p.gender)
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return name+"-"+age+"-"+gender;
	}
	
}

class Student extends Person{
	
	String stuNo;
	String clazz;
	
	public Student(String name, int age, boolean gender, String stuNo, String clazz) {
		super(name,age,gender);
		this.stuNo=stuNo;
		this.clazz=clazz;
	}
	
	@Override
	public String toString() {
		
		return super.toString()+"-"+stuNo+"-"+clazz;
	}
	
	
	@Override
	public boolean equals(Object o) {
		
		if(!(super.equals(o)))
			return false;
		
		if(this==o)
			return true;
		
		if(!(o instanceof Student))
			return false;
		Student p=(Student)o;
		if(stuNo!=p.stuNo||clazz!=p.clazz)
			return false;
		
		return true;
	}
}

class Company{
	
		String name;
		
		public Company(String name) {
			this.name 

 

=name;
		}
		
		public boolean equals(Company o) {
			if(name==o.name 

 

)
				return true;
			return false;
		}
		
		@Override
		public String toString() {
			
			return name;
		}
}

class Employee extends Person{
	
	Company company;
	double salary;

	public Employee(String name, int age, boolean gender, double salary, Company company) {
		
		super(name,age,gender);
		this.salary=salary;
		this.company=company;
		
	}
	
	public String toString() {
		return super.toString()+"-"+company+"-"+salary;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		return true;
	}
	


	
	
}