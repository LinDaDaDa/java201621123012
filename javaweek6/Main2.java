package javaweek6;

import java.util.Scanner;

class ArrayUtils{
	static PairResult pr = new PairResult();
	 static class PairResult{
		 private double min;
		 private double max;
		 
		 public static PairResult findMinMax(double[] values){
				for (int i = 0; i < values.length; i++) {
					for (int j = 0; j < values.length-1; j++) {
						   if(values[j]>values[j+1])
						   {
						     double temp=values[j];
						      values[j]=values[j+1];   
						      values[j+1]=temp;
						   }
					}
					pr.max=values[values.length-1];
					pr.min=values[0];
				}
				return pr;
				
			}
		
		
		@Override
		public String toString() {
			return "PairResult [min=" + min + ", max=" + max + "]";
		}
		
		 
	}

}

public class Main2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n =sc.nextInt();
		
		ArrayUtils.PairResult m = new ArrayUtils.PairResult();
		
		double [] a = new double[n];
		for (int i = 0; i < n; i++) {
			a[i]=sc.nextDouble();
		}
		m=ArrayUtils.PairResult.findMinMax(a);
		System.out.println(m.toString());
		System.out.println(ArrayUtils.PairResult.class);
	}
}
