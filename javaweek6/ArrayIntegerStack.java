package javaweek6;

import java.util.Arrays;
import java.util.Scanner;



interface IntegerStack {
	public Integer push(Integer item);//如果item为null，则不入栈直接返回null。如果栈满，也返回null。如果插入成功，返回item。
	public Integer pop();   //出栈，如果为空，则返回null。出栈时只移动栈顶指针，相应位置不置为null
	public Integer peek();  //获得栈顶元素，如果为空，则返回null.
	public boolean empty(); //如果为空返回true
	public int size();      //返回栈中元素个数
}


public class ArrayIntegerStack {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		IntegerStack1 sdm = new IntegerStack1();

	
	int a =sc.nextInt();
	for (int i = 0; i < a; i++) {
		Integer b= new Integer(sc.nextInt());
		 System.out.println(sdm.push(b));
	}
	System.out.println(sdm.peek()+","+sdm.empty()+","+sdm.size());
	System.out.println(Arrays.toString(IntegerStack1.q));
	int x =sc.nextInt();
	for (int i = 0; i < x; i++) {
		System.out.println(sdm.pop());
		
	}
	System.out.println(sdm.peek()+","+sdm.empty()+","+sdm.size());
	System.out.println(Arrays.toString(IntegerStack1.q));
	}
}

class IntegerStack1 implements IntegerStack{
	private int flag=0;
	static Scanner sc = new Scanner(System.in);
	
	static Integer[] q = new Integer[sc.nextInt()];
	
	@Override
	public Integer push(Integer item) {
		// TODO Auto-generated method stub
		if(item==null)
		return null;
		else if(q[q.length-1] != null)
			return null;
		else
		{	
			for(int i=0; i<q.length;i++)
			{
				if(q[i]==null){
					q[i] = item;
					break;
				}
			}
			return item;
	}
	}
	@Override
	public Integer pop() {
		int i= 0;
		if(q[0]==null)
			return null;
		for(i=0; i<q.length;i++)
		{
			if(q[i]==null){
				
				break;
				
			}
		}
		
		if(q[i-flag-1]==null)
			return null;
		else
		{
			flag++;
			return q[i-flag];
		}
		// TODO Auto-generated method stub
	
	}

	@Override
	public Integer peek() {
		int i=0;
		if(q[0]==null)
			return null;
		else{
			for(i=0; i<q.length;i++)
			{
				if(q[i]==null){
					
					break;
					
				}
			}}
		return q[i-1-flag];
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		int i=0;
		for(i=0; i<q.length;i++)
		{
			if(q[i]==null){
				
				break;
				
			}
		}
		if(q[i-1-flag]==null)
		return true;
		else
			return false;
	}

	@Override
	public int size() {
		int i=0;
		for( i=0; i<q.length;i++)
		{
			if(q[i]==null){
				
				break ;
				
			}
		}
		return i-flag;
	}
	
}