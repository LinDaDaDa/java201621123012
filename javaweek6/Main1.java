package javaweek6;



import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class PersonSortable2{
	String name;
	 int age;
	
	public PersonSortable2(String name, int age) {
		
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return  name + "-" + age ;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	



	
}


public class Main1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n= sc.nextInt();
		PersonSortable2[] stus = new PersonSortable2[n];
	for (int i = 0; i < n; i++) {
		stus[i] = new PersonSortable2(sc.next(),sc.nextInt());
	}
	   
		Arrays.sort(stus,new Comparator<PersonSortable2>(){

			@Override
			public int compare(PersonSortable2 o1, PersonSortable2 o2) {
				// TODO Auto-generated method stub
				return  o1.name.compareTo(o2.name);
			}
			
			
		}
		);
		System.out.println("NameComparator:sort");
		for (int i = 0; i < n; i++) {
			System.out.println(stus[i]);
		}
	
		Arrays.sort(stus, (o1,o2)->o1.age-o2.age);
		System.out.println("AgeComparator:sort");
		for (int i = 0; i < n; i++) {
		System.out.println(stus[i]);
		
	}
		System.out.println("201621123012 ��־ΰ");

}
	}
